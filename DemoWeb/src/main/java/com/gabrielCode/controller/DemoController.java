package com.gabrielCode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gabrielCode.model.Persona;
import com.gabrielCode.model.Usuario;
import com.gabrielCode.repo.IPersonaRepo;
import com.gabrielCode.repo.IUsuarioRepo;

import es.edu.alter.practica0.modelo.PiedraPapelTieraFactory;

@RestController
public class DemoController {
	@Autowired
//	private IUsuarioRepo usuRepo;
	private IPersonaRepo repo;
	
//	@Autowired
//	@Qualifier("piedra")
//	private PiedraPapelTieraFactory piedra;
	
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
//		System.out.println("piedra=" + piedra.getNombre());
		if(name.endsWith("World")) {
	//		repo.delete(new Persona(3, null));
			name="Gabrielito 2022";
		}
			
		Persona per = new Persona(0, name);
		repo.save(per);
//		usuRepo.save(new Usuario(0, "clave", "usuario"));
		
		model.addAttribute("name", name);
		
		return "greeting";
	}
	

}
